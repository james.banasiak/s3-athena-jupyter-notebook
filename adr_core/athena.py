
import sys
from datetime import date, timedelta, datetime
import uuid
import re
from boto3 import client
from boto3 import Session
import tempfile
import time
import pandas as pd

class AWS_Athena:
    def __init__(self, params):
        self.params = params
        self.session = Session(profile_name=self.params['profile'], region_name=self.params['region'])

    def get_var_char_values(self, d):
        return [obj['VarCharValue'] if 'VarCharValue' in obj else None for obj in d['Data']]

    def query(self, query, wait=True):
        client = self.session.client('athena')

        ## get the query execution ID
        response_query_execution_id = client.start_query_execution(
            QueryString=query,
            QueryExecutionContext={
                'Database': self.params['database']
            },
            ResultConfiguration={
                'OutputLocation': 's3://' + self.params['bucket'] + '/' + self.params['path']
            }
        )

        if not wait:
            return response_query_execution_id['QueryExecutionId']
        else:
            response_get_query_details = client.get_query_execution(
                QueryExecutionId=response_query_execution_id['QueryExecutionId']
            )
            status = 'RUNNING'
            iterations = 360  # 30 mins
            while (iterations > 0):
                iterations = iterations - 1
                response_get_query_details = client.get_query_execution(
                    QueryExecutionId=response_query_execution_id['QueryExecutionId']
                )
                status = response_get_query_details['QueryExecution']['Status']['State']
                if (status == 'FAILED') or (status == 'CANCELLED'):
                    failure_reason = response_get_query_details['QueryExecution']['Status']['StateChangeReason']
                    print(failure_reason)
                    return False, False

                elif status == 'SUCCEEDED':
                    location = response_get_query_details['QueryExecution']['ResultConfiguration']['OutputLocation']

                    ## Function to get output results
                    response_query_result = client.get_query_results(
                        QueryExecutionId=response_query_execution_id['QueryExecutionId']
                    )
                    result_data = response_query_result['ResultSet']
                    # create s3 client to download to local drive
                    s3 = self.session.client('s3')
                    f = tempfile._get_default_tempdir() + "/" + next(tempfile._get_candidate_names())
                    s3.download_file(self.params['bucket'], self.params['path'] + '/' + response_query_execution_id[
                        'QueryExecutionId'] + ".csv", f)
                    return f

                else: # RUNNING...
                    time.sleep(5)
            return False
